LIBS:=`pkg-config --libs libosmocore`
CFLAGS:=-g -Wall `pkg-config --cflags libosmocore`


osmo-logbench: osmo-logbench.o
	$(CC) $(CFLAGS) -o $@ $^ $(LIBS)

%.o: %.c
	$(CC) -o $@ -c $^
