#include <stdlib.h>

#include <osmocom/core/logging.h>
#include <osmocom/core/application.h>

#define MAX_LINE_LEN	512

static unsigned int gen_len(void)
{
	int rnd;
	do {
		rnd = rand() % MAX_LINE_LEN;
	} while (rnd <= 0);
	return rnd;
}

static void fill_buf(char *buf, unsigned int len)
{
	unsigned int i;
	for (i = 0; i < len; i++)
		buf[i] = 'a' + (i % 25);
	buf[len] = '\0';
}

static void print_random_log(void)
{
	char buf[MAX_LINE_LEN+1];
	fill_buf(buf, gen_len());

	LOGP(DLGLOBAL, LOGL_ERROR, "stress: %s\n", buf);
}
int main(int argc, char **argv)
{
	osmo_init_logging2(NULL, NULL);

#if 0
	log_target_destroy(osmo_stderr_target);
	struct log_target *tgt = log_target_create_gsmtap("localhost", 4729, "logbench", 0, 1);
	log_add_target(tgt);
	log_set_all_filter(tgt, 1);
#endif

	//for (int i = 0; i < 1000000; i++) {
	while (1) {
		print_random_log();
	}
}
